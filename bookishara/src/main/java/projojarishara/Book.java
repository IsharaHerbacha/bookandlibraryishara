package projojarishara;

import net.tncy.validator.constraints.books.ISBN;

public class Book {
    public String title;
    public String author;

    @ISBN
    public String isbn;

    public Book(String title,String author,String isbn){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }

    public Book(){
        this.title = "title";
        this.author = "author";
        this.isbn = "null";
    }

}
